<?php

namespace App\Providers;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        view()->composer('threads.partials.sidebar', function ($view) {
            $subjects = \App\Models\Subject::all();
            $view->with(compact('subjects'));
        });

        view()->composer('layouts.partials.navbar', function($view) {
            $user = auth()->user();
            $view->with(compact('user'));
        });
    }
}