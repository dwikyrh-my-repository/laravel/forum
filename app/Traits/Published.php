<?php

namespace App\Traits;

Trait Published
{
    public function getPublishedAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}