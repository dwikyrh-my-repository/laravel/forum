<?php
namespace App\Traits;

use App\Models\Thread;

trait HasManyThread {
    public function threads() {
        return $this->hasMany(Thread::class);
    }
}
