<?php

namespace App\Models;

use App\Traits\BelongsToUser;
use App\Traits\Published;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Thread extends Model
{
    use HasFactory, Published, BelongsToUser;
    protected $table = 'threads';
    protected $withCount = ['replies'];
    protected $with = ['subject', 'user'];
    protected $fillable = [
        'subject_id', 'title', 'slug', 'body',
    ];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}