<?php

namespace App\Models;

use App\Traits\HasManyThread;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory, HasManyThread;
    protected $table = 'subjects';
    protected $withCount = ['threads'];
    
    public function threads()
    {
        return $this->hasMany(Thread::class)->orderBy('created_at', 'desc');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}