<?php

namespace App\Models;

use App\Traits\BelongsToUser;
use App\Traits\Published;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    use HasFactory, Published, BelongsToUser;
    protected $table = 'replies';
    protected $with = ['user', 'thread'];
    protected $fillable = [
        'user_id', 'hash', 'body'
    ];

    public function thread()
    {
        return $this->belongsTo(Thread::class);
    }

    public function getRouteKeyName()
    {
        return 'hash';
    }
}