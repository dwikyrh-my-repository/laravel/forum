<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use App\Models\Subject;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(User $user)
    {
        $threads = Thread::all();
        $users = User::all();
        $replies = Reply::all();
        $subjects = Subject::all();
        return view('home', compact('user', 'threads', 'users', 'replies', 'subjects'));
    }
}