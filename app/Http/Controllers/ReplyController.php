<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    # middleware
    public function __construct()
    {
        $this->middleware('auth')->except(['topUser']);
    }

    # most user replies / top users
    public function topUser(User $user)
    {
        # get data user most replies
        $users = User::has('replies')->orderBy('replies_count', 'desc')->take(25)->get();
        $thread = Thread::all();

        #return view
        return view('replies.top-users', compact('users', 'thread'));
    }

    # method for create reply
    public function store(Request $request, Thread $thread)
    {
        # validation
        $this->validate($request, [
            'body' => ['required'],
        ]);

        # action create reply
        $thread->replies()->create([
            'user_id' => auth()->user()->id,
            'body' => $request->body,
            'hash' => str()->random(32),
        ]);

        # redirect
        return back()->with('success', 'Your reply has been created.');
    }

    # view edit reply
    public function edit(Reply $reply)
    {
        # policy
        $this->authorize('update', $reply);

        # return view
        return view('replies.edit', compact('reply'));
    }

    # method for update reply
    public function update(Request $request, Reply $reply)
    {
        # policy
        $this->authorize('update', $reply);

        # validation
        $this->validate($request, [
            'body' => 'required',
        ]);

        # action update reply
        $reply->update([
            // 'user_id' => auth()->user()->id,
            'body' => $request->body,
            'hash' => str()->random(32),
        ]);

        # redirect
        return to_route('threads.show', $reply->thread)->with('success', 'Your reply has been updated.');
    }

    # method for delete reply
    public function destroy(Reply $reply)
    {
        # policy
        $this->authorize('delete', $reply);

        # action delete reply
        $reply->delete();

        #redirect
        return back()->with('success', 'Your reply has been deleted.');
    }
}