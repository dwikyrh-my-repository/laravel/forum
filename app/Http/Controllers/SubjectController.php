<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use App\Models\Subject;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    # middleware
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    # all data subjects
    public function index(Thread $thread, Subject $subject, User $user)
    {
        # show threads by subjects
        $threads = $subject->threads()->latest()->when(request()->q, function ($threads) {
            $threads->where('title', 'like', '%' . request()->q . '%');
        })->paginate(20);

        #return view
        return view('subjects.index', compact('subject', 'threads'));
    }

    # show threads
    public function show(Subject $subject, Thread $thread)
    {
        # get replies by threads
        $replies = $thread->replies()->paginate(20);

        # return view
        return view('subjects.show', compact('subject', 'thread', 'replies'));
    }

    # view edit threads
    public function edit(Subject $subject, Thread $thread)
    {
        # policy
        $this->authorize('update', $thread);

        # get all data subject
        $subjects = Subject::all();

        # return view
        return view('subjects.edit', compact('subject', 'thread', 'subjects'));
    }
}