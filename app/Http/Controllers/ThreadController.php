<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    # middleware
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'popular', 'show', 'unanswered');
    }

    # method for get all threads
    public function index(Thread $thread, Subject $subject)
    {
        # show threads with function a search threads
        $threads = Thread::latest()->when(request()->q, function ($threads) {
            $threads = $threads->where('title', 'like', '%' . request()->q . '%')
                ->orWhereRelation('user', 'name', 'like', '%' . request()->q . '%');
        })->paginate(20);

        # return view
        return view('threads.index', compact('threads', 'thread', 'subject'));
    }

    # threads mine
    public function mine(Thread $thread)
    {
        # get all data threads by user
        $threads = auth()->user()->threads()->latest()->paginate(20);

        # return view
        return view('threads.index', compact('threads', 'thread'));
    }

    # threads popular
    public function popular()
    {
        # get all data popular by lots of replies
        $threads = Thread::has('replies')->orderBy('replies_count', 'desc')->paginate(20);

        # return view
        return view('threads.index', compact('threads'));
    }

    # threads unanswered
    public function unanswered()
    {
        # get data unanswered when it has no threads
        $threads = Thread::doesntHave('replies')->orderByDesc('created_at')->paginate(20);

        # return view
        return view('threads.index', compact('threads'));
    }

    # threads answered
    public function answered(Thread $thread)
    {
        # get data answered by logged in user
        $threads = Thread::has('replies')->whereRelation('replies', 'user_id', auth()->user()->id)
            ->orderByDesc('created_at')->paginate(20);

        #return view
        return view('threads.index', compact('threads'));
    }

    # view create thread
    public function create()
    {
        # get data thread and subjects
        $thread = new Thread;
        $subjects = Subject::all();

        # return view
        return view('threads.create', compact('thread', 'subjects'));
    }

    # action to create thread
    public function store(Request $request)
    {
        # validation
        $this->validate($request, [
            'title' => ['required'],
            'body' => ['required'],
            'subject' => ['required'],
        ]);

        # action create thread
        auth()->user()->threads()->create([
            // 'user_id' => auth()->user()->id,
            'title' => $request->title,
            'slug' => str($request->title . ' ' . str()->random(5))->slug(),
            'body' => $request->body,
            'subject_id' => $request->subject,
        ]);

        #redirect
        return to_route('threads.index')->with('success', 'Your thread was created.');
    }

    # view show
    public function show(Thread $thread, Subject $subject, User $user)
    {
        # get data threads spesific with replies
        $replies = $thread->replies()->paginate(20);

        # return view
        return view('threads.show', compact('thread', 'replies', 'subject', 'user'));
    }

    # view edit thread
    public function edit(Thread $thread)
    {
        # policy
        $this->authorize('update', $thread);

        # get subject data
        $subjects = Subject::all();

        # return view
        return view('threads.edit', compact('thread', 'subjects'));
    }

    # action to update thread
    public function update(Request $request, Thread $thread)
    {
        # policy
        $this->authorize('update', $thread);

        # validation
        $this->validate($request, [
            'title' => ['required'],
            'body' => ['required'],
            'subject' => ['required'],
        ]);

        # action update thread
        $thread->update([
            'title' => $request->title,
            'slug' => str($request->title . ' ' . str()->random(5))->slug(),
            'body' => $request->body,
            'subject_id' => $request->subject,
        ]);

        # redirect
        return to_route('threads.show', $thread)->with('success', 'Your thread has been updated.');
    }

    # action to delete thread
    public function destroy(Thread $thread)
    {
        # policy
        $this->authorize('delete', $thread);

        # action to delete
        $thread->delete();

        # redirect
        return to_route('threads.mine')->with('success', 'Your thread has been deleted.');
    }
}