<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use App\Models\Thread;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    # middleware
    public function __construct()
    {
        $this->middleware('auth')->except('show');
    }

    # show data user with thread
    public function show(User $user, Thread $thread)
    {
        # get data threads by user
        $threads = $user->threads()->latest()->paginate(20);

        # return view
        return view('threads.user', compact('threads', 'user', 'thread'));
    }

    # view edit user
    public function edit(User $user)
    {
        # policy
        $this->authorize('update', $user);

        # view
        return view('users.edit', compact('user'));
    }

    # method for update profile
    public function updateProfile(Request $request, User $user)
    {
        # policy
        $this->authorize('updateProfile', $user);

        # avatar upload handle
        $fileRequest = $request->file('avatar');

        if ($request->hasFile('avatar')) {
            if ($user->avatar)
            {
                Storage::delete($user->avatar);
                $avatar = $fileRequest->storeAs('images/users', $fileRequest->hashName());
            } else {
                $avatar = $user->avatar;
            }
        } else {
            $avatar = $user->avatar;
        }
        # validation
        $request->validate([
            'avatar' => ['nullable', 'image', 'max:2048', 'mimes:png,jpg,jpeg'],
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'alpha_num', 'max:30', 'min:3', Rule::unique('users', 'username')->ignore($request->user())],
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users', 'email')->ignore($request->user())],
        ]);

        # action update user profile
        $user->update([
            'avatar' => $avatar,
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
        ]);

        # redirect;
        return to_route('users.edit', $user)->with('success', 'Your profile has been updated.');
    }

    # method for update password
    public function updatePassword(Request $request, User $user)
    {
        # policy
        $this->authorize('updatePassword', $user);

        # validation
        $request->validate([
            'current_password' => ['required', 'min:8', 'max:50'],
            'password' => ['required', 'confirmed', 'min:8', 'max:50'],
        ]);

        # match old password
        if (!Hash::check($request->current_password, auth()->user()?->password)) {
            throw ValidationException::withMessages([
                'current_password' => 'These credentials do not match our records.'
            ]);
        }

        # match the new password and old password (dont same)
        if (Hash::check($request->password, auth()->user()?->password)) {
            throw ValidationException::withMessages([
                'password' => 'The new password cannot be the same as the old password.'
            ]);
        }
        
        # update the new password
        User::whereId(auth()->user()->id)->update([
            'password' => Hash::make($request->password)
        ]);

        # redirect
        return back()->with('success', 'Your password has been changed.');
    }
}
