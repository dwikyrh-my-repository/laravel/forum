@extends('layouts.app', ['title' => "Edit Your Thread - GreenK Forum"])
@section('content')
<div class="container">
  <div class="row">
    {{-- sidebar --}}
    <div class="col-md-4 ">
      @include('threads.partials.sidebar')
    </div>

    <div class="col-md-8">
      @include('alerts')
      <div class="card">
        <div class="card-header">
          Edit Your Thread
        </div>
        <div class="card-body">
          <div class="row">
            {{-- button update thread --}}
            <form action="{{ route('threads.update', $thread) }}" method="POST" class="mb-3">
              @csrf
              @method('PUT')
              @include('threads.form', ['submit' => 'Update'])
            </form>
            {{-- button delete thread --}}
            <form action="{{ route('threads.destroy', $thread) }}" method="POST" style="display: inline">
              @csrf
              @method('DELETE')
              <button type="submit" class="btn btn-danger btn-md">Delete</button>
            </form>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection
