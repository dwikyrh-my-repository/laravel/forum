@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
@endpush
@extends('layouts.app', ['title' => "$thread->title - GreenK Forum"])
@section('content')
<div class="container">
  {{-- search --}}
  <x-search></x-search>

  <div class="row">
    {{-- sidebar --}}
    <div class="col-md-4">
      @include('threads.partials.sidebar')
    </div>

    <div class="col-md-8">
      <div class="card card-body mb-2">
        <div class="d-flex">
          <div class="flex-shrink-0">
            {{-- avatar user --}}
            <a href="{{ route('users.show', $thread->user) }}">
              <img src="{{ asset($thread->user->avatar()) }}" class="rounded-circle me-3" style="width: 70px; height: 70px;">
            </a>
          </div>
          <div class="flex-grow-1">
            {{-- thread title --}}
            <h5 class="mt-0">{{ $thread->title }}</h5>
            <span class="threads-published" style="color: #545454">
              {{ $thread->published }} &middot;
              <a href="{{ route('users.show', $thread->user) }}" class="text-decoration-none">
                {{ $thread->user->username }} &middot;
              </a>
              {{-- subject --}}
              <a href="{{ route('subjects.index', ['subject' => $thread->subject, 'thread' => $thread]) }}" class="text-decoration-none">
                {{ $thread->subject->name }}
              </a>
              @can('update', $thread)
              &middot;
              <small> <a href="{{ route('threads.edit', $thread) }}" class="text-decoration-none">edit</a></small>
              @endcan
            </span>
          </div>
        </div>
        {{-- thread body --}}
        <p class="mt-3">
          {!! nl2br($thread->body) !!}
        </p>
      </div>

      {{-- reply --}}
      @if ($replies->count())
      @foreach ($replies as $reply)
      <div class="card card-body border-start shadow-sm border-0 mb-2">
        <div class="replies d-flex px-2">
          {{-- user avatar --}}
          <div class="flex-shrink-0 py-2">
            <img src="{{ asset($reply->user->avatar()) }}" class="rounded-circle me-3" style="width: 50px; height: 50px;">
          </div>
          <div class="flex-grow-1 mt-1 overflow-auto">
            {{-- reply body --}}
            <div class="reply-body w-100 ">
              {!! nl2br($reply->body) !!}
            </div>
            <small class="text-muted">
              {{-- user --}}
              <a href="{{ route('users.show', $reply->user) }}" class="text-decoration-none">
                {{ $reply->user->username }}
              </a>
              &middot; {{ $reply->published }}
              {{-- button update reply --}}
              @can('update', $reply)
              &middot;
              <a href="{{ route('replies.edit', $reply) }}" class="text-decoration-none">edit</a>
              @endcan
              {{-- button delete reply --}}
              @can('delete', $reply)
              &middot;
              <form action="{{ route('replies.destroy', $reply) }}" method="POST" class="d-inline">
                @csrf
                @method('DELETE')
                <button type="danger" class="border-0 bg-white text-danger">delete</button>
              </form>
              @endcan
            </small>
          </div>
        </div>
      </div>
      @endforeach
      @endif

      {{-- pagination --}}
      <div class="pagination d-flex justify-content-end mt-4">
        {{ $replies->links() }}
      </div>

      {{-- form reply --}}
      @auth
      <div class="card card-body mt-2 sticky-top">
        <form action="{{ route('replies.store', $thread) }}" method="POST">
          @csrf
          {{-- reply thread --}}
          <div class="mb-3">
            <textarea name="body" class="form-control" id="body" cols="30" rows="4" placeholder="Reply here..."></textarea>
          </div>
          {{-- button submit --}}
          <div class="text-end">
            <button type="submit" class="btn btn-primary btn-md">Reply</button>
          </div>
        </form>
      </div>
      @else
      <a href="{{ route('login') }}" class="text-primary text-decoration-none">Login first to reply to the thread.</a>
      @endauth

    </div>
  </div>
</div>
@endsection
@push('scripts')
@include('alerts')
@endpush
