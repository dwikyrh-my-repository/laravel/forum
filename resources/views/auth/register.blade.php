@extends('layouts.app', ['title' => 'Register - GreenK Forum'])
@section('content')
<section id="register">
  <div class="container d-flex align-items-center justify-content-center">
    <div class="col-12">
      <div class="row g-0 d-flex align-items-center justify-content-center">
        {{-- image --}}
        <img src="{{ asset('assets/img/bird.jpg') }}" style="width: 445px" class="d-none d-lg-flex image-fluid shadow rounded-start">
        <div class="col-lg-5">
          <div class="card-body py-4 px-md-4 shadow rounded" style="height: 612px">
            {{-- header text --}}
            <h5 class="text-center fw-bold fs-2">REGISTER</h5>

            {{-- form register --}}
            <form method="POST" action="{{ route('register') }}" class="px-4">
              @csrf

              {{-- name --}}
              <div class="row">
                <label for="name" class="col-form-label">{{ __('Name') }}</label>
                <div class="col-12 col-md-12">
                  <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus>
                  @error('name')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              {{-- username --}}
              <div class="row">
                <label for="username" class="col-form-label">{{ __('Username') }}</label>
                <div class="col-12 col-md-12">
                  <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" autocomplete="username" autofocus>
                  @error('username')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              {{-- email --}}
              <div class="row">
                <label for="email" class="col-form-label">{{ __('Email Address')}}</label>
                <div class="col-12 col-md-12">
                  <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email">
                  @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              {{-- password --}}
              <div class="row">
                <label for="password" class="col-form-label">{{ __('Password')}}</label>
                <div class="col-12 col-md-12">
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              {{-- confirm password --}}
              <div class="row mb-3">
                <label for="password-confirm" class="col-form-label">{{ __('Confirm Password') }}</label>
                <div class="col-12 col-md-12">
                  <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                </div>
              </div>

              {{-- button submit --}}
              <div class="row mb-0">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-primary">
                    {{ __('Register') }}
                  </button>
                </div>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
