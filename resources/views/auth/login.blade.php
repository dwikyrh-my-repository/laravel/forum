@extends('layouts.app', ['title' => 'Login - GreenK Forum'])
@section('content')
<section id="login">
  <div class="container d-flex align-items-center justify-content-center">
    <div class="col-12">
      <div class="row g-0 d-flex align-items-center justify-content-center">
        {{-- image --}}
        <img src="{{ asset('assets/img/bird.jpg') }}" style="width: 445px" class="d-none d-lg-flex image-fluid shadow rounded-start">
        <div class="col-lg-5">
          <div class="card-body py-4 px-md-4 shadow rounded" style="height: 612px;">
            {{-- header text --}}
            <h5 class="text-center fw-bold fs-2" style="margin-top: 120px">LOGIN</h5>

            {{-- form login --}}
            <form action="{{ route('login') }}" method="POST" class="px-4">
              @csrf

              {{-- email address --}}
              <div class="row">
                <label for="email" class="col-12 col-form-label">{{ __('Email Address') }}</label>
                <div class="col-12 col-md-12">
                  <input id="text" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                  @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              {{-- password --}}
              <div class="row mb-3">
                <label for="password" class="col-12 col-form-label">{{ __('Password') }}</label>
                <div class="col-12 col-md-12">
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="current-password">
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              {{-- remember me --}}
              <div class="row mb-3">
                <div class="col-md-12">
                  <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="form-check-label" for="remember">
                      {{ __('Remember Me') }}
                    </label>
                  </div>
                </div>
              </div>

              {{-- button login and forgot password --}}
              <div class="row mb-0">
                <div class="col-md-10">
                  <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                  </button>
                  @if (Route::has('password.request'))
                  <a class="btn text-primary" href="{{ route('password.request') }}">
                    {{ __('Forgot Your Password?') }}
                  </a>
                  @endif
                </div>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
