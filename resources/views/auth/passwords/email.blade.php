@extends('layouts.app', ['title' => 'Reset Password - GreenK Forum'])
@section('content')
<section id="email">
  <div class="container d-flex align-items-center justify-content-center">
    <div class="col-12">
      <div class="row g-0 d-flex align-items-center justify-content-center">
        {{-- image --}}
        <img src="{{ asset('assets/img/bird.jpg') }}" alt="" style="width: 445px" class="d-none d-lg-flex image-fluid shadow rounded-start">
        <div class="col-lg-5">
          <div class="card-body py-4 px-md-4 shadow rounded" style="height: 612px;">
            {{-- header text --}}
            <h5 class="text-center fw-bold fs-2" style="margin-top: 150px">RESET PASSWORD</h5>

            {{-- alert --}}
            @if (session('status'))
            <div class="alert alert-success" role="alert">
              {{ session('status') }}
            </div>
            @endif

            {{-- form email for reset password --}}
            <form method="POST" action="{{ route('password.email') }}" class="px-4">
              @csrf

              {{-- email address --}}
              <div class="row mb-3">
                <label for="email" class="col-form-label">{{ __('Email Address') }}</label>
                <div class="col-md-12">
                  <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
                  @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
              </div>

              {{-- button submit --}}
              <div class="row">
                <div class="col-md-12">
                  <button type="submit" class="btn btn-primary">
                    {{ __('Send Password Reset Link') }}
                  </button>
                </div>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
