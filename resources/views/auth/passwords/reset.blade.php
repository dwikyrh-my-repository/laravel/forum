@extends('layouts.app', ['title' => 'Reset Password - GreenK Forum'])
@section('content')
<section id="reset-password">
  <div class="container d-flex align-items-center justify-content-center">
    <div class="col-12">
      <div class="mb-3 mt-3">
        <div class="row g-0 d-flex align-items-center justify-content-center">
          {{-- image  --}}
          <img src="{{ asset('assets/img/bird.jpg') }}" alt="" style="width: 375px" class="d-none d-lg-flex image-fluid shadow rounded-start">
          <div class="col-lg-5">
            <div class="card-body py-4 px-md-4 shadow rounded" style="height: 515px;">
              {{-- header text --}}
              <h5 class="text-center fw-bold fs-2" style="margin-top: 60px">RESET YOUR PASSWORD</h5>

              {{-- form reset password --}}
              <form method="POST" action="{{ route('password.update') }}" class="px-4">
                @csrf
                {{-- token --}}
                <input type="hidden" name="token" value="{{ $token }}">

                {{-- email address --}}
                <div class="row mb-3">
                  <label for="email" class="col-form-label">{{ __('Email Address') }}</label>
                  <div class="col-md-12">
                    <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" autocomplete="email" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>

                {{-- new password --}}
                <div class="row mb-3">
                  <label for="password" class="col-form-label">{{ __('Password') }}</label>
                  <div class="col-md-12">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>

                {{-- password confirm --}}
                <div class="row mb-3">
                  <label for="password-confirm" class="col-form-label">{{ __('Confirm Password') }}</label>
                  <div class="col-md-12">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password">
                  </div>
                </div>

                {{-- button submit --}}
                <div class="row mb-0">
                  <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                      {{ __('Reset Password') }}
                    </button>
                  </div>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
