@extends('layouts.app', ['title' => 'Dashboard - GreenK Forum'])

@section('content')
<div class="container">
  <div class="row justify-content-evenly mt-5">
    {{-- threads --}}
    <div class="card text-bg-dark mb-4" style="max-width: 18rem;">
      <div class="card-body text-center">
        <h5 class="card-title fw-bold">THREADS</h5>
        <p class="card-text fw-bolder mt-3 fs-2">
          {{ $threads->count() }}
        </p>
      </div>
    </div>
    {{-- subjects --}}
    <div class="card text-bg-dark mb-4" style="max-width: 18rem;">
      <div class="card-body text-center">
        <h5 class="card-title fw-bold">SUBJECTS</h5>
        <p class="card-text fw-bolder mt-3 fs-2">
          {{ $subjects->count() }}
        </p>
      </div>
    </div>
    {{-- replies --}}
    <div class="card text-bg-dark mb-4" style="max-width: 18rem;">
      <div class="card-body text-center">
        <h5 class="card-title fw-bold">REPLIES</h5>
        <p class="card-text fw-bolder mt-3 fs-2">
          {{ $replies->count() }}
        </p>
      </div>
    </div>
    {{-- users --}}
    <div class="card text-bg-dark mb-4" style="max-width: 18rem;">
      <div class="card-body text-center">
        <h5 class="card-title fw-bold">USERS</h5>
        <p class="card-text fw-bolder mt-3 fs-2">
          {{ $users->count() }}
        </p>
      </div>
    </div>
  </div>
</div>
@endsection
