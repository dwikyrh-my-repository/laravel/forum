<nav class="navbar navbar-expand-md navbar-light bg-light shadow-sm">
  <div class="container">
    {{-- app name or brand --}}
    <a class="navbar-brand" href="{{ route('threads.index') }}">
      {{ config('app.name', 'Greenk Forum') }}
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <!-- Right Side Of Navbar -->
      <ul class="navbar-nav ms-auto">
        <!-- Authentication Links -->
        <li class="nav-item">
          <a href="{{ route('threads.index') }}" class="nav-link {{ request()->is('forums*') ? 'active' : '' }} {{ request()->is('subjects*') ? 'active' : '' }}
          {{ request()->is('forum*') ? 'active' : '' }}">
            Forum
          </a>
        </li>
        @guest
        {{-- login --}}
        @if (Route::has('login'))
        <li class="nav-item">
          <a class="nav-link {{ request()->routeIs('login') ? 'active' : '' }}" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @endif

        {{-- register --}}
        @if (Route::has('register'))
        <li class="nav-item">
          <a class="nav-link {{ request()->routeIs('register') ? 'active' : '' }}" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
        @endif
        @else

        {{-- dropdown link --}}
        <li class="nav-item dropdown">
          {{-- name --}}
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }}
          </a>
          <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
            {{-- profile --}}
            <a href="{{ route('users.edit', $user) }}" class="dropdown-item">
              Profile
            </a>

            {{-- logout --}}
            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
            </form>

          </div>
        </li>
        @endguest
      </ul>

    </div>
  </div>
</nav>
