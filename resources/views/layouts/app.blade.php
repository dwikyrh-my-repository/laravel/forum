<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- csrf token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ $title ?? 'GreenK Forum' }}</title>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;700&display=swap" rel="stylesheet">
  <link rel="shortcut icon" href="{{ asset('assets/img/logo-greenk.ico') }}" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">
  @stack('styles')
  <!-- Scripts -->
  @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<body style="background-color:#f7f9f9">
  <div id="app" style="min-height: calc(100vh - 40px);">
    @include('layouts.partials.navbar')
    <main class="py-4">
      @yield('content')
    </main>
  </div>

  {{-- footer --}}
  <footer class="bg-light text-center p-2 border-top">
    <span class="text-dark">
      <span class="fw-bolder">&copy; 2022 GreenK Forum</span>. All Rights Reserved.
    </span>
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/js/all.min.js"></script>
  @stack('scripts')
</body>
</html>
