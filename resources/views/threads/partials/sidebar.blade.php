{{-- threads --}}
<div class="accordion" id="accordion">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThreads" aria-expanded="true" aria-controls="collapseThreads">
        Threads
      </button>
    </h2>
    <div id="collapseThreads" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordion">

      {{-- create thread --}}
      @auth
      <a href="{{ route('threads.create')}}" class="text-decoration-none text-dark {{ request()->routeIs('threads.create') ? 'active' : '' }}">
        <div class="accordion-body">
          Create New Thread <i class="fa-solid fa-plus float-end"></i>
        </div>
      </a>
      @endauth

      {{-- all threads --}}
      <a href="{{ route('threads.index')}}" class="text-decoration-none text-dark
      {{ request()->is('forums*') ? 'active' : '' }} {{ request()->routeIs('users.show') ? 'active' : '' }}">
        <div class="accordion-body">
          All Threads <i class="fa-regular fa-comments float-end"></i>
        </div>
      </a>

      {{-- user threads --}}
      @auth
      <a href="{{ route('threads.mine') }}" class="text-decoration-none text-dark {{ request()->is('forum/mine') ? 'active' : '' }}">
        <div class="accordion-body">
          My Threads <i class="fa-regular fa-message float-end"></i>
        </div>
      </a>
      @endauth

      {{-- popular threads --}}
      <a href="{{ route('threads.popular') }}" class="text-decoration-none text-dark {{ request()->is('forum/popular') ? 'active' : '' }}">
        <div class="accordion-body">
          Popular Threads <i class="fa-solid fa-fire-flame-curved float-end"></i>
        </div>
      </a>

      {{-- unanswered question --}}
      <a href="{{ route('threads.unanswered') }}" class="text-decoration-none text-dark {{ request()->is('forum/unanswered') ? 'active' : '' }}">
        <div class="accordion-body">
          Unanswered Question <i class="fa-solid fa-ban float-end"></i>
        </div>
      </a>

      {{-- The Threads You Answered --}}
      @auth
      <a href="{{ route('threads.answered') }}" class="text-decoration-none text-dark {{ request()->is('forum/answered') ? 'active' : '' }}">
        <div class="accordion-body">
          The Threads You Answered <i class="fa-regular fa-paper-plane float-end"></i>
        </div>
      </a>
      @endauth

      {{-- Top Users --}}
      <a href="{{ route('replies.top-user') }}" class="text-decoration-none text-dark {{ request()->is('replies/top-user') ? 'active' : '' }}">
        <div class="accordion-body">
          Top Users <i class="fa-solid fa-users float-end"></i>
        </div>
      </a>

    </div>
  </div>
</div>


<div class="accordion mt-3 mb-3" id="accordionSubjects">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button text-white" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSubjects" aria-expanded="true" aria-controls="collapseSubjects">
        Subjects
      </button>
    </h2>
    <div id="collapseSubjects" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionSubjects">
      @foreach ($subjects as $subject)
      <a href="{{ route('subjects.index', $subject) }}" class="text-decoration-none text-dark">
        <div class="accordion-body {{ request()->is("subjects/" . $subject->slug . "*") ? 'active' : '' }}">
          {{ $subject->name }}
          <span class="badge bg-primary rounded-pill float-end">{{ $subject->threads_count }}</span>
        </div>
      </a>
      @endforeach
    </div>
  </div>
</div>

{{-- form new thread --}}
@auth('web')
@endauth
