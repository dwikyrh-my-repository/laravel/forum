{{-- title --}}
<div class="mb-3">
  <label for="title" class="form-label">Title</label>
  <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" id="title" value="{{ old('title', $thread->title) }}" placeholder="Title...">
  @error('title')
  <div class="invalid-feedback">{{ $message }}</div>
  @enderror
</div>

{{-- subject --}}
<div class="mb-3">
  <label for="subject" class="form-label">Subject</label>
  <select name="subject" class="form-select @error('subject') is-invalid @enderror" id="subject">
    <option @selected(true) @disabled(true)>Choose one</option>
    @foreach ($subjects as $subject)
    <option @selected(old('subject')==$subject->id || $thread?->subject_id == $subject->id) value="{{ $subject->id
            }}">{{ $subject->name }}</option>
    @endforeach
  </select>
  @error('subject')
  <div class="invalid-feedback">{{ $message }}</div>
  @enderror
</div>

{{-- body --}}
<div class="mb-3">
  <label for="body" class="form-label">Body</label>
  <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="body" cols="30" rows="10" placeholder="Create a thread here...">{{ old('body', $thread->body) }}</textarea>
  @error('body')
  <div class="invalid-feedback">{{ $message }}</div>
  @enderror
</div>

{{-- button submit --}}
<button type="submit" class="btn btn-md btn-primary">{{ $submit }}</button>
