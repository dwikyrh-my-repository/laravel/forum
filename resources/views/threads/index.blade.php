@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
@endpush
@extends('layouts.app', ['title' => 'Threads - GreenK Forum'])
@section('content')
<div class="container">
  {{-- search --}}
  <x-search></x-search>
  <div class="row">
    {{-- sidebar --}}
    <div class="col-md-4">
      @include('threads.partials.sidebar')
    </div>
    <div class="col-12 col-md-8">
      <div class="card-body">
        @forelse ($threads as $thread)
        <a href="{{ route('threads.show', $thread) }}" class="text-decoration-none threads py-2 mb-4 shadow-sm">
          <div class="threads-body">
            <div class="threads-title">{{ $thread->title }}</div>
            <small class="threads-published">
              {{ $thread->published }} &middot; {{ $thread->user->username }} &middot; {{ $thread->subject->name }}
              &middot; {{ $thread->replies_count }} Replies
            </small>
          </div>
        </a>
        @empty
        <div class="alert alert-info mt-4 mt-sm-0">No data threads.</div>
        @endforelse
      </div>

      {{-- pagination --}}
      <div class="mt-2 d-flex justify-content-end">
        {{ $threads->links() }}
      </div>

    </div>
  </div>
</div>
@endsection
@push('scripts')
@include('alerts')
@endpush
