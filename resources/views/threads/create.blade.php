@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
@endpush
@extends('layouts.app', ['title' => 'Threads - GreenK Forum'])
@section('content')
<div class="container">
  {{-- search --}}
  <x-search></x-search>
  <div class="row">
    {{-- sidebar --}}
    <div class="col-md-4">
    @include('threads.partials.sidebar')
    </div>
    <div class="col-12 col-md-8">
      <div class="card">
        <div class="card-header bg-primary">
            Create new thread
        </div>
        <div class="card-body">
            {{-- form create thread --}}
          <form action="{{ route('threads.store') }}" method="POST">
            @csrf
            @include('threads.form', ['submit' => 'Thread'])
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
@include('alerts')
@endpush
