@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
@endpush
@extends('layouts.app', ['title' => "$user->name (@$user->username) - GreenK Forum"])
@section('content')
<div class="container">
  {{-- search --}}
  <x-search></x-search>
  <div class="row">
    {{-- sidebar --}}
    <div class="col-md-4">
      @include('threads.partials.sidebar')
    </div>

    <div class="col-12 col-md-8">
      {{-- user info --}}
      <div class="card mb-3">
        <div class="card-body">
          <div class="d-flex">
            {{-- avatar user --}}
            <div class="flex-shrink-0">
              <img src="{{ asset($user->avatar()) }}" style="width: 70px; height: 70px" class="rounded">
            </div>
            {{-- user info name, username, threads count, and replies count --}}
            <div class="flex-grow-1 ms-3">
              <h5>{{ $user->name  }}</h5>
              <small> {{ __('@') . $user->username }} &middot; Threads ({{ $user->threads_count }}) Replies ({{ $user->replies_count }})</small>
            </div>
          </div>
        </div>
      </div>

      {{-- threads --}}
      <div class="card-body">
        @forelse ($threads as $thread)
        <a href="{{ route('threads.show', $thread) }}" class="text-decoration-none threads py-2 mb-4 shadow-sm">
          <div class="threads-body">
            <div class="threads-title">{{ $thread->title }}</div>
            <small class="threads-published">
              {{ $thread->published }} &middot;
              {{ $thread->user->name }} &middot; {{ $thread->subject->name }}
              &middot; {{ $thread->replies_count }} Replies
            </small>
          </div>
        </a>
        @empty
        <div class="alert alert-info">No data threads.</div>
        @endforelse
      </div>

      {{-- pagination --}}
      <div class="mt-2 d-flex justify-content-end">
        {{ $threads->links() }}
      </div>

    </div>
  </div>
</div>
@endsection
@push('scripts')
@include('alerts')
@endpush
