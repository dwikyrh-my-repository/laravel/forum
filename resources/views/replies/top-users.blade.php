@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
@endpush
@extends('layouts.app', ['title' => 'Top Users - GreenK Forum'])
@section('content')
<div class="container">
  {{-- search --}}
  <x-search></x-search>
  <div class="row">
    {{-- sidebar --}}
    <div class="col-md-4">
      @include('threads.partials.sidebar')
    </div>
    <div class="col-12 col-md-8">
      @forelse ($users as $user)
      <div class="card card-body mb-3">
        <div class="row">
          <div class="col-md-6">
            <div class="d-flex">
              <div class="flex-shrink-0">
                <img src="{{ asset($user->avatar()) }}" alt="{{ $user->name }}" class="rounded-circle" style="width: 85px; height: 85px;">
              </div>
              <div class="flex-grow-1 ms-3 mt-2">
                <h4>{{ $user->name }}</h4>
                <a href="{{ route('users.show', $user) }}" class="text-decoration-none">
                  {{ __('@') . $user->username }} &middot;
                </a>
                Replies ({{ $user->replies_count }})
              </div>
            </div>
          </div>

          <div class="col-md-6 text-end px-4">
            <h1>#{{ 1 + $loop->index }}</h1>
          </div>
        </div>
      </div>
      @empty
      <div class="alert alert-info">
        No data top users.
      </div>
      @endforelse
    </div>
  </div>
</div>
@endsection
@push('scripts')
@include('alerts')
@endpush
