@extends('layouts.app', ['title' => 'Edit Your Reply - GreenK Forum'])
@section('content')
<div class="container">
  {{-- search --}}
  <x-search></x-search>
  <div class="row">
    {{-- sidebar --}}
    <div class="col-md-4">
      @include('threads.partials.sidebar')
    </div>

    <div class="col-md-8">
      <div class="card">
        <div class="card-header bg-primary text-white">
          Edit your reply in thread: {{ $reply->thread->title }}
        </div>
        <div class=" card-body">
          {{-- form edit reply --}}
          <form action="{{ route('replies.update', $reply) }}" method="POST">
            @csrf
            @method('PUT')
            {{-- body --}}
            <div class="mb-3">
              <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="body" cols="30" rows="5">{{ old('body', $reply->body) }}</textarea>
              @error('body')
              <div class="invalid-feedback">{{ $message }}</div>
              @enderror
            </div>
            {{-- button submit --}}
            <button type="submit" class="btn btn-primary btn-md">Update reply</button>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection
