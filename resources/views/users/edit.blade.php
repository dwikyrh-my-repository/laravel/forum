@push('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
@endpush
@extends('layouts.app', ['title' => "$user->name Settting - GreenK Forum"])
@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-4">
      @include('threads.partials.sidebar')
    </div>
    <div class="col-12 col-md-8 card card-body">
      {{-- image --}}
      <div class="col-12-mt-4 d-flex justify-content-center rounded-circle">
        <img src="{{ $user->avatar() }}" alt="" class="img-fluid rounded-circle" style="width: 150px; height: 150px;">
      </div>

      {{-- user update profile --}}
      <div class="col-12  mt-4 card">
        {{-- card header --}}
        <div class="card-header bg-primary text-white">
          Edit Profile
        </div>
        <div class="card-body">
          <div class="flex-grow-1">
            {{-- form edit profile --}}
            <form action="{{ route('users.update-profile', $user) }}" method="POST" enctype="multipart/form-data">
              @csrf
              @method('PUT')

              {{-- image --}}
              <div class="mb-3">
                <label for="avatar" class="form-label">Avatar</label>
                <input type="file" name="avatar" id="avatar" class="form-control">
              </div>

              {{-- username --}}
              <label for="username" class="form-label">Username</label>
              <div class="mb-3 input-group">
                <span class="input-group-text" id="addOne">@</span>
                <input type="text" name="username" id="username" class="form-control @error('username') is-invalid @enderror" value="{{ old('username', $user->username) }}">
                @error('username')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>

              {{-- name --}}
              <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" name="name" id="" class="form-control @error('name') is-invalid @enderror" value="{{ old('name', $user->name) }}" placeholder="Name...">
                @error('name')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>

              {{-- email --}}
              <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email', $user->email) }}" placeholder="Email...">
                @error('email')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>

              {{-- button submit --}}
              <button class="btn btn-primary btn-md">Save</button>
            </form>

          </div>
        </div>
      </div>

      {{-- update password --}}
      <div class="col-12 mt-4 card">
        {{-- card header --}}
        <div class="card-header bg-primary text-white">
          Update Password
        </div>
        <div class="card-body">
          {{-- form update password --}}
          <form method="POST" action="{{ route('users.update-password', $user) }}">
            @csrf
            @method('PUT')

            {{-- current password --}}
            <div class="mb-3">
              <label for="current-password" class="form-label">Current Password</label>
              <input type="password" name="current_password" class="form-control @error('current_password') is-invalid @enderror" id="current-password" autocomplete="current-password" />
              @error('current_password')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>

            {{-- new password --}}
            <div class="mb-3">
              <label for="new-password" class="form-label">New Password</label>
              <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="new-password" autocomplete="new-password" />
              @error('password')
              <div class="invalid-feedback">
                {{ $message }}
              </div>
              @enderror
            </div>

            {{-- confirm password --}}
            <div class="mb-3">
              <label for="password-confirmation" class="form-label">Confirm Password</label>
              <input type="password" name="password_confirmation" class="form-control" id="password-confirmation" autocomplete="new-password" />
            </div>

            {{-- button submit --}}
            <div class="mb-0">
              <button class="btn btn-primary btn-md" type="submit">
                Update password
              </button>
            </div>
          </form>

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
@include('alerts')
@endpush
