<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
<script>
    @if(session('success'))
        Toastify({
        text: '{{ session('success') }}',
        duration: 3500,
        newWindow: true,
        close: true,
        gravity: "top", // `top` or `bottom`
        position: "right", // `left`, `center` or `right`
        stopOnFocus: true, // Prevents dismissing of toast on hover
        style: {
            background: "#1AD51A",
        },
        onClick: function(){} // Callback after click
        }).showToast();
    @endif
</script>

