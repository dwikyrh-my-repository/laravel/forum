<div class="row d-flex justify-content-center mb-4">
  <div class="col-12 col-lg-8 mt-3 mt-md-0">
    <form action="{{ route('threads.index') }}" method="GET">
      <div class="input-group">
        <input name="q" type="search" class="form-control form-control-md" placeholder="Search here..." aria-label="Search..." aria-describedby="button-searc-threads" value="{{ request('q') }}">
        <button class="btn btn-outline-primary btn-md" type="submit" id="button-search-threads">
          <i class="fa-solid fa-magnifying-glass"></i>
        </button>
      </div>
    </form>
  </div>
</div>
