<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'name' => 'HTML',
            'slug' => 'html',
        ]);
        Subject::create([
            'name' => 'CSS',
            'slug' => 'css',
        ]);
        Subject::create([
            'name' => 'JavaScript',
            'slug' => 'javascript',
        ]);
        Subject::create([
            'name' => 'PHP',
            'slug' => 'php',
        ]);
        Subject::create([
            'name' => 'Boostrap',
            'slug' => 'bootstrap',
        ]);
        Subject::create([
            'name' => 'Vue',
            'slug' => 'vue',
        ]);
        Subject::create([
            'name' => 'React',
            'slug' => 'react',
        ]);
        Subject::create([
            'name' => 'Node',
            'slug' => 'node',
        ]);
        Subject::create([
            'name' => 'MongoDB',
            'slug' => 'mongodb',
        ]);
        Subject::create([
            'name' => 'Angular',
            'slug' => 'angular',
        ]);
        Subject::create([
            'name' => 'Laravel',
            'slug' => 'laravel',
        ]);
        Subject::create([
            'name' => 'CodeIgniter',
            'slug' => 'codeigniter',
        ]);
        Subject::create([
            'name' => 'MySQL',
            'slug' => 'mysql',
        ]);
        Subject::create([
            'name' => 'PostgreSQL',
            'slug' => 'postgresql',
        ]);
        Subject::create([
            'name' => 'Python',
            'slug' => 'python',
        ]);
        Subject::create([
            'name' => 'Other',
            'slug' => 'other',
        ]);
    }
}