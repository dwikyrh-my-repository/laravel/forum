<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ReplyController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\ThreadController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [HomeController::class, 'index']);

# Users
Route::controller(UserController::class)->group(function() {
    Route::get('/users/{user}', 'show')->name('users.show');
    Route::get('/users/{user}/edit', 'edit')->name('users.edit');
    Route::put('/users/{user}/update-profile', 'updateProfile')->name('users.update-profile');
    Route::put('/users/{user}/update-password', 'updatePassword')->name('users.update-password');
});

# Threads
Route::controller(ThreadController::class)->group(function() {
    Route::get('/forums', 'index')->name('threads.index');
    Route::get('/forum/mine', 'mine')->name('threads.mine');
    Route::get('/forum/popular', 'popular')->name('threads.popular');
    Route::get('/forum/unanswered', 'unanswered')->name('threads.unanswered');
    Route::get('/forum/answered', 'answered')->name('threads.answered');
    Route::get('/forum/create', 'create')->name('threads.create');
    Route::post('/forums', 'store')->name('threads.store');
    Route::get('/forums/{thread}', 'show')->name('threads.show');
    Route::get('/forums/{thread}/edit', 'edit')->name('threads.edit');
    Route::put('/forum/{thread}', 'update')->name('threads.update');
    Route::delete('/forum/{thread}', 'destroy')->name('threads.destroy');
});

# Replies
Route::controller(ReplyController::class)->group(function() {
    Route::get('/replies/top-user', 'topUser')->name('replies.top-user');
    Route::post('/replies/{thread}', 'store')->name('replies.store');
    Route::get('/replies/{reply}/edit', 'edit')->name('replies.edit');
    Route::put('/replies/{reply}', 'update')->name('replies.update');
    Route::delete('/replies/{reply}', 'destroy')->name('replies.destroy');
});

# Subjects
Route::controller(SubjectController::class)->group(function () {
    Route::get('/subjects/{subject}', 'index')->name('subjects.index');
    Route::get('/subjects/{subject}/{thread}', 'show')->name('subjects.show');
    Route::get('/subjects/{subject}/{thread?}/edit', 'edit')->name('subjects.edit');
});